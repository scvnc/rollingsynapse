<?php

class GroupsController extends Controller {

   /* main method
    * 
    * Executed when the url is ?group and has a non-method argument (such as an id)
    * Branches to other methods that access different views.
    * 
    */
    public function main() {
		
		// If there arent any arguments ( ?groups/ ) then branch to the viewAll method.
	    if (sizeof($this->args) == 0)
			 $this->viewAll();
		
		// Otherwise treat the first argument as a group id
		else
		    $this->GroupInfo($this->args[0]);
	   

    }
	
	/* viewAll method:
	 * 
	 * Displays all the groups that exist in the database
	 *
	 */
	public function viewAll(){
	
		$group = new GroupModel();
		
		$this->groupQuery = $group->viewAll();
		
		$this->deploy();		
	
	     
	}
	
	
	/* GroupInfo method
	 * 
	 * Obtains the data needed from the model layer for displaying group information
	 * and prepares for the view to format the data.
	 * 
	 * 
	 * 
	 * 
	 */
	public function GroupInfo($id, $msg = NULL){
	
		$group = new GroupModel();
		$mMod  = new MembershipModel();
      
		$this->id = $id;
		
		
		if($msg != NULL)
		  $this->msg = $msg;
		
		try {
			
			$this->groupInfo = $group->getSingle($id);
			$this->groupRoster = new GroupRoster($id);
         
		} catch (Exception $e) {
			
			$this->error($e->getMessage());
			return;
			
		}
	    
		// ACCESS CONTROL: Only members can see working workflows.
        //                 Only attach workingWorkflwos if access is granted.
		
	    if ($this->isMember = ($mMod->roleLevel($id) != -1) )
	        $this->workingWorkflows = $group->getWorkflows($id, false);
			
		$this->userRole = $mMod->role( $mMod->roleLevel($id) );	
	    $this->isAdmin = $mMod->isAdmin($id);   
		
		
		// Always get public workflows: everyone can see these.
		$this->publishedWorkflows = $group->getWorkflows($id, true);
		
		$this->groupCreator = $group->getCreator($id);
		
		$this->isPrivate = $this->groupInfo->private;
		
        //Check to see if current group has a SuperGroup and if so displays it
        if ( $superGroup = $group->viewFamilyGroups($id) )
            {
            $this->superGroupName = $superGroup->name;
            $this->superGroupID = $superGroup->idGroup;
            }
            

		$this->deploy();
	}
	
	//Error method to print a message if the user tries to access
	//forbiden methods or pages
	private function error($msg) {
		
		//$this->setTitle("error");
		
		$this->msg = $msg;
		$this->deploy();
	}

	
	
	/*	edit method
		
		Gives the admin of each group the access to edit the information of a group
		SQL statment updates the database group table
	
	*/
	
	public function edit(){

	     
      $mMod = new MembershipModel();
	  $gm = new GroupModel();
      
		// ACCESS CONTROL 
		//No edit can be done if user is not an admin
      if( ! $mMod->isAdmin($this->args['1']) )
         $this->error("You have no permission to edit this group!");
         
      
		//Checks if the user doesn't change anything
		if(!isset($_POST['name'])) 
		{
         $gid = $this->getArg(1);
         $this->groupInfo =$gm->getSingle($gid);
		 $this->deploy();
		}    
      
		$SQL = "UPDATE `Group`
				 SET `name` = :name,
				     `shortDesc` = :shortDesc,
				     `longDesc` = :longDesc,
				     `private` = :private
			     WHERE createdBy = :id AND idGroup = :gid";


	    $statement = GroupModel::$database->prepare($SQL);
		
		
		$array[':name'] = $_POST['name'];		
		$array[':shortDesc'] = $_POST['shortDesc'];		
		$array[':longDesc'] = $_POST['longDesc'];		
		$array[':private'] = isset($_POST['private']) ? true : false;		
		$array[':id'] = $_SESSION['uid'];
		$array[':gid'] = $this->args['1'];
		
		
		$statement->execute( $array );
		
		header('location: ?groups/'. $this->args['1']);
	
	    $this->deploy();
		
	}
	
	
	/*
	 *	Create method
	 *
	 *	Executes the SQL statement to insert a new group table into the database
	 *
	 */
	
	public function create(){
	
		//if no name is set, the page redeploys without creating a group
		if(!isset($_POST['name'])) 
		{
			$this->deploy();
		}
      
      $gm = new GroupModel();
      $mm = new MembershipModel();
      
      // Are we making this a subgroup?
      if ($_POST['supGroup'] != "") {
         
         $superGID = $gm->groupID($_POST['supGroup']);
         
         if( !$mm->isAdmin($superGID) )
            throw new RuntimeException("You cannot make this subGroup.");
      
      } else {
         $_POST['subGroup'] = NULL;
      }
      
      $gm->create( $_POST['name'], 
                   $_POST['shortDesc'], 
                   $_POST['longDesc'], 
                   isset($_POST['private']) ? true : false, 
                   $superGID );
				 
             
		header('location: ?groups');
		

	}
	
	
	
   /*
	*	join method
	*	
	*	Method to add a member in a group if the group has public access
	*	by adding the user to the Membership table of the database
	*	
	*/	
	public function join(){
	   
	   $gid = $this->args[1];
	   $group = new GroupModel();
	   
	   //Gets the value of the private attribute for each group
	   $this->isPrivate = $group->getSingle($gid)->private;
	   
	   //ACCESS CONTROL
	   //As long as the group is no private, user can join the group
	   if($this->isPrivate == false)
	       $SQL = "INSERT INTO `Membership`
				(`idGroup`,
				 `idScientist`,
				 `joinDate`,
				 `idGroupRole`)
				  VALUES
				 (:idGroup,
				  :idScientist,
				  now(),
				  :role)";
				  
		$statement = GroupModel::$database->prepare($SQL);
		
		$array[':idGroup'] = $gid;
		$array[':idScientist'] = $_SESSION['uid'];
		$array[':role'] = '2';
				
		$statement->execute( $array );	  
				  
	  	$this->groupInfo($gid, 'Successfuly joined the group!');
	}
	
	/****************************
	 * START JSON ACCESS METHODS
	 ****************************/
	
	
	/* public json method
    * 
    * It's kind of like a sub url router for json access to models.
    * 
    */
	public function json() {
      
		$action = $this->getArg(1);
		$gm = new GroupModel();
    $mMod = new MembershipModel();
               $aMod = new AuthenticationModel();
		
		
		// Prepare an object for JSON encoding.
		$obj = new stdClass();
		$obj->success = FALSE;
		
		
		try {
		   
			switch($action) {
			   
            case "addMember":
               $uid = $this->getArg(2, true);
            
               $gid = $this->getArg(3, true);
               

      
               
               // ACCESS CONTROL
               // If you're not admin AND
               // it is not the case that you are trying to join a public group: get upset.
               
               
               // If you're not an admin..
               if (!$mMod->isAdmin($gid)) {
                  
                  // AND you're not trying to add yourself to a public group...
                  if ( ! ($uid == $aMod->getAuthUID() && !$gm->isPrivateGroup()) )
                     throw new RuntimeException("You do not have permission to do this.");
                  
               }
      
             
             
               $obj->success = $gm->addMember($uid, $gid);
               
               break;
            
            
            case "delMember":
               $uid = $this->getArg(2, true);
            
               $gid = $this->getArg(3, true);
               
               if ( ! $mMod->isAdmin($gid) ){
                  
                  if( $uid != $aMod->getAuthUID() )
                     throw new RuntimeException("You can't delete this member!!!!");
                  
                 }
               
            
               $obj->success = $gm->delMember($uid, $gid);
               
               break;
            
            
            
            case "modRole":
               $uid = $this->getArg(2, true);
            
               $gid = $this->getArg(3, true);
               
               $rid = $this->getArg(4, true);
               
               $obj->success = $gm->changeMemberRole($uid, $gid, $rid);
               
               break;
               
            case "list":
            
               $name = isset($_GET['term']) ? $_GET['term'] : NULL;
               $obj = $gm->viewAllJSON($name);
               
               break;
            
            
            default:
               break;
            
			}
			
		} catch (Exception $e) {
			$obj->msg = $e->getMessage();
		}
		
		
		echo json_encode($obj);
		
      // Get out of the framework-- output nothing more.
		throw new ExitException;
		
	}
	

   
}

?>
