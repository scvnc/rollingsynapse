<?php

/* RegistrationContoller class
 * 
 * The Controller defines accepts values from the view through @_POST
 * The values from @_POST are checked to make sure they are valid 
 *     and if they are added to the database
 * 
 * If any of the values are invalid the Controller displays an error 
 *     message and asks the user to reEnter the values
 */
 
class RegistrationController extends Controller 
    {
    
    /* main method
     * 
     * Calls registraion form and process
     * 
     * Returns: nothing
     * Throws: Exception if @newEmail is already in database
     * 
     */
    public function main() 
        {
        $this-> registrationForm();
        $this-> process();  
        }
    
    /* registrationForm method
     * 
     * Calls registraionForm view file
     * 
     * 
     */
    private function registrationForm()
        {    
        $this->deploy();
        }
    
    /* errorRegistrationForm method
     * 
     * Calls errorRegistraionForm view file
     *  
     */
    private function errorRegistrationForm($msg, $postAr)
        {
        $this->postAr =$postAr;
        $this->msg = $msg;    
        $this->deploy();
        }
 
    /* process method
     * 
     * Handles the calling the various error checking methods
     *  
     */
    public function process() 
       {
        $this->isPostEmpty();
        
        $name = $_POST["name"];
        $title = $_POST["title"];       
        $email_1 = $_POST["email_1"];
        $email_2 = $_POST["email_2"];
        $password = $_POST["password"];
        
        $this->emailValidate($email_1, $email_2 );
        $this->duplicateCheck($email_1);
        $this->nameValidate($name);
        
        $SQL = "INSERT INTO Scientist ( name, title, email, password, JoinDate)
        VALUES (:name, :title, :email, SHA1(:password), now() )";
        
        $statement = RegistrationModel::$database->prepare($SQL);
    
        $array[':name'] = $name;
        $array[':title'] = $title;
        $array[':email'] = $email_1;
        $array[':password'] = $password;
        
        $statement->execute( $array );
                     
        $this->deploy();
        }
    
    /* isPostEmpty method
     * 
     * Checks whether any of the $_POST variables are empty other than title
     *  
     */
    private function isPostEmpty()
        {
        if (empty($_POST["name"]))
            $this->error("Please Enter A Value into Name");
          
        if (empty($_POST["email_1"]))
            $this->error("Please Enter A Value into Email");
            
        if (empty($_POST["email_2"]))
            $this->error("Please Enter A Value in Re-Enter Email");
            
        if (empty($_POST["password"]))
            $this->error("Please Enter A Value into Password");
            
        return;
        }
    
    /* isPostEmpty method
     * 
     * Checks whether @email_1 and @email_2 are the same
     *    and if they are valid email addresses
     *  
     * Parameters: @email_1, @email_2
     */    
    private function emailValidate($email_1, $email_2)
        {
        if($email_1 != $email_2)
            {
            $this->error("Your emails do not match. Please try again.");
            }
        if(!filter_var($email_1, FILTER_VALIDATE_EMAIL))
            {
            $this->error("Invalid Email Address, Try Agian");
            }
        }
    
    /* nameValidate method
     * 
     * Checks whether @email_1 and @email_2 are the same
     *    and if they are valid email addresses
     *  
     * Parameters: @email_1, @email_2
     */    
    private function nameValidate( &$name)
        {
        if (preg_match('#[0-9]#', $name))
            { 
            $this->error("Invalid Name, Can not have Numbers"); 
            }
        else
            { 
            return; 
            } 
        }
    
    /* duplicateCheck method
     * 
     * Checks whether @email is already registered
     *      Creates a new RegistrationModel instance and calls RegistrationModel::canAdd  
     *
     * Parameters: @email
     *  Catches: exception from RegistrationModel::canAdd if email is already registered
     */    
    private function duplicateCheck($email)
        {
        $rm = new RegistrationModel();
      
        try
            {
            $rm->canAdd($email);
            }
        catch (Exception $e) 
            {
            $this->error($e->getMessage());
            return;
            }
        }
        
    /* error method
     * 
     *  Stores @_POST data in an array and calls the errorRegistrationForm
     *   sends @postAr and @msg to errorRegistrationForm
     *
     * Parameters: @msg
     */   
     private function error($msg) 
        {
        $postAr = array(
            "name" => $_POST["name"],
            "title" => $_POST["title"],       
            "email_1" => $_POST["email_1"],
            "email_2" => $_POST["email_2"],
            );
        
        //adds enlarges and centers msg
        $msg = "<h3 align='center'>".$msg."</h3>";
                
        $this->msg = $msg;
        $this->errorRegistrationForm($msg, $postAr);
        }
    }