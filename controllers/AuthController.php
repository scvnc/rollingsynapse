<?php

class AuthController extends Controller {

	public function main() {
	
	// is logged in?
		$auth = new AuthenticationModel();
	
		if($auth->IsAuthenticated()) {
			$this->logout();
		} else {
			$this->loginForm();
		}
		
	}
	
	private function loginForm($msg = "") {
	
		$auth = new AuthenticationModel();
		
		if ($msg != "")
			$this->errorMsg = $msg;
			
		if ($auth->IsAuthenticated()) {
			$this->error("You are already logged in.");
		}
		
		
		$this->deploy();
		
	}
	
	public function login()
	{
		$auth = new AuthenticationModel();
		
		// If there are no post variables, call the login form.
		if (!isset($_POST['login']) && !isset($_POST['pw']) ){
			$this->loginForm();
		}
			
		$login = $_POST['login']; //theoretically here and the line below are a good place to control input strings
		$pw = $_POST['pw']; //and protect against injection
		
		
		
		try
		{

			if ($row = $auth->matchCredentials($login, $pw)) 
			{
				$_SESSION['uid'] = $row->idScientist; //success! id stored in the first position is now the session id
				$_SESSION['name'] = $row->name; //fill in the name
				
				$this->success();
				
			} else {
			
				$this->loginForm("Invalid credentials, try again");
			}
			
		} catch (PDOException $e) {
		
			$this->error("Database Exception: " . $e->getMessage() );
		
		}
			
		
	}
	
	public function logout() {
	
		$auth = new AuthenticationModel();
		
		$auth -> logout();
		
		// Redirect to home
		header("Location: ?");
	
	}
	
	
	private function success() {
		
		// Redirect to home
		header("Location: ?");
		
	}
	
	private function error($msg) {
		
		$this->setTitle("error");
		
		$this->msg = $msg;
		$this->deploy();
	}

}