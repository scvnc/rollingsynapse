<?php
class WorkflowController extends Controller {
      
/* Workflow Controller: Main View 
   This View displays a list of Published Workflows
 */ 
public function main() {
   
    
	$workflows = new WorkflowModel();
	$this->publishedWorkflows = $workflows->get(NULL, NULL, TRUE, NULL);
	$this->deploy();
      
      
    
}

/*
Function: ViewWorkflow
Purpose:  This function displays a single workflow 
Arguments: None
*/
      
public function viewWorkflow() {
   

   
   $idWorkflow = $this->getArg(1);
   
   $workflow = new WorkflowModel();
   $mMod = new MembershipModel();
   
   $gid = $workflow->getGroupID($idWorkflow);
   
   $this->workflowInfo = $workflow->viewWorkflow($idWorkflow); 
      
      // Added the check to see if it's published. -Vince
		if( !$this->workflowInfo->published && $mMod->roleLevel($gid) == -1)
		{
			throw new RuntimeException("You do not have permission to view this workflow");
		}
	
	
		


	$this->deploy();
  }
   
/* Edit function   */
/*	
This function allows for the information in a working workflow to be edited.
The User must be a member of the group 	
	 */   
   public function edit() {
   

		$mMod = new MembershipModel();
      $wMod = new WorkflowModel();
      
      $group = $wMod->getGroupID($this->args['1']);
      
      
      // ACCESS CONTROL
      if ($mMod->roleLevel($group) > 1 || $mMod->roleLevel($group) == -1){
         throw new RuntimeException("You cannot edit this workflow!");
      }
	

    if(!isset($_POST['title']))
		{
		   $this->workflowInfo = $wMod->get($this->getArg(1));
         $this->workflowInfo = $this->workflowInfo[0];
         
			$this->deploy();
		}

		$SQL = "UPDATE `Workflow`
				 SET `title` = :title,
				     `content` = :content,
				     `longDesc` = :longDesc
			     WHERE `idWorkflow` = :idWorkflow";


	    $statement = WorkflowModel::$database->prepare($SQL);
		
		$array[':title'] = $_POST['title'];
		$array[':longDesc'] = $_POST['longDesc'];
		$array[':content'] = $_POST['content'];
		$array[':idWorkflow'] = $this->args['1'];
		
		
		$statement->execute( $array );
	
		header('location: ?Workflow/viewWorkflow/'. $this->args['1']);
		
	 
	    $this->deploy();
		
	
	
	  
	  
      
   }
   
//Deletes a Workflow / Not Implemented
   public function delete() {
      
      
      
   }
   
 

   
}
?>
