<?php
/* GroupRoster Helper
 * 
 * Aids in formatting the roster list on group informatino pages.
 * 
 * Instantiate the object with the group id you wish to query for members
 * and print out the table by calling the printRoster method.
 */
class GroupRoster {
	
	private $gid; // Group ID
	private $gm;  // Group Model
	
	private $roster; // Array of members.
	private $roles;  // Array of roles.
	
	
	
	public function __construct($gid) {
		
		$this->gid = $gid;
      
		$this->gm = new GroupModel();
		$this->mm = new MembershipModel();
      $this->am = new AuthenticationModel();
		
		// Load roles
		$this->roles = $this->mm->rawROLES;
		
		// Load roster for this group.
		$this->roster = $this->gm->getRoster($this->gid);
		
	}
	
	// Prints the roster element.
	public function printRoster() {
		
      echo "<div id='roster' gid='$this->gid'>";
      
		echo "<table id='roster'>
		        <tr>
		          <th>Name </th>
		          <th>Role </th>
		          <th>Join Date</th>
		          <th>Options</th>
		        </tr>";
				
		foreach ($this->roster as $member){
			
			echo "<tr uid='$member->idScientist'>
			       <td>". $member->name ."</td>";
                
			echo  "<td>";
			         $this->printRoleDropdown($member);
			echo  "</td>";
         
			echo  "<td>". $member->joinDate . "</td>";
         
			echo  "<td>"; 
			       $this->printOptionButtons($member);
			echo  "</td>
			      </tr>";
				
		}
		
		echo "</table>";
      
      $this->printAddBox();
         
		echo "</div>";
	}
   
   
   // Prints the dropdown menu for changing roles
   private function printRoleDropdown($member) {
      
      if ( !$this->mm->isAdmin($this->gid) ) {
         echo $member->roleName;
         return;
      }
      
      echo "<select uid='$member->idScientist' gid='$this->gid'>";
      foreach($this->roles as $role) {
         
         echo '<option value="'.$role->idGroupRole.'" class="roleDd" ';
         echo ($member->roleName == $role->name) ? 'selected="yes"':"";
         echo ' >'.$role->name.'</option>';
         
      }
      echo "</select>";
      
      
   }
   
   // Prints the options buttons, revoke for now.
   private function printOptionButtons($member) {
      
      
      // If the user isn't an admin...
      if ( !$this->mm->isAdmin($this->gid) ) {
         
         // And the button being printed isn't for themselves.
         if( $member->idScientist != $this->am->getAuthUID() )
            return;
         
      }
         
      
      // Revoke button;
      echo '<a class="btn btn-danger btn-small" href="#" onClick="revokeUser('.$member->idScientist.', '.$this->gid.')"><i class="icon-remove-circle icon-white"></i> Revoke</a></button>';
      
      
   }
   
   private function printAddBox() {
      
      if ( !$this->mm->isAdmin($this->gid) )
         return;
      
         echo "<div id='roster-add-search'>";
            echo "Add User: <input id='roster-add-search' type='text' />";
         echo "</div>";
      
   }
	
	
	
}

?>