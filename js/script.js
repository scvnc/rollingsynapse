
/* * * * * * * * * * * * * *
 * GROUP ROSTER FUNCTIONS  *
 * * * * * * * * * * * * * */
 
/* changeGroupRole
 * 
 * @int uid
 *    UserID to change role.
 * 
 * @int gid
 *    GroupID to change user's role in.
 * 
 * @int rid
 *    RoleID to switch to.
 * 
 * @param selector
 *    reference to the selector html element
 * 
 * JSON call to change group role.
 * 
 */
function changeGroupRole(uid, gid, rid, selector) {
   
   
   selector.disabled = true;           
   
   $.getJSON('?groups/json/modRole/'+uid+'/'+gid+'/'+rid, function(result) {
      
      if(!result.success) {
         
         alert(result.msg)
         selector.value = selector.origValue;
      }
   
   selector.disabled = false; 
      
   })
   
}

function revokeUser(uid, gid) {
   
   $.getJSON('?groups/json/delMember/' + uid + '/' + gid, function(result){
      
      if(result.success == true) {
         $("tr[uid="+uid+"]").remove();
      } else {
         alert(result.msg);
      }
      
   });

}
function findUID(name, callback) {
   

   $.ajax("?scientist/json", {type:'POST', 
                              success: callback,
                              data: { name: name }
                             });

   
   
}

function attachSelectEvents() {
   
      $("table#roster tr td select").each(function() { 
      
      this.origValue = this.value;
      $(this).change( function(){
         
         changeGroupRole(this.getAttribute('uid'),
                         this.getAttribute('gid'), 
                         this.value,
                         this);
         
         }); 
      
   });
   
}

function addUser(uid, gid) {
   
   
   $.getJSON('?groups/json/addMember/' + uid + '/' + gid, function(result) {
      location.reload(true)
   });
   
   
}

function createRow(uid, name, gid, role) {
   
   var tr = document.createElement('tr');
       tr.setAttribute('uid', uid);
   
   var td_name = document.createElement('td');
   $(td_name).html(name);

   var td_role = document.createElement('td');
   $(td_role).html();
   
}

/* * * * * * * * * * * * * *
 * GROUP CREATE FUNCTIONS  *
 * * * * * * * * * * * * * */



$(document).ready(function(){
   
   // If there is a table with the id of roster
   if ($("table#roster").length > 0){
      attachSelectEvents()
   
      $("input#roster-add-search").autocomplete( { source: "?scientist/json/", 
                                                   select: function(event, ui) { 
                                                         addUser(ui.item.value, 
                                                                  $("div#roster")[0].getAttribute('gid') ) 
                                                         }
                                                });
   }
   
   // If there is a form with the id of 
   if ($("form#createGroup").length > 0){
            $("input#roster-add-search").autocomplete( { source: "?groups/json/list/", });
   
   }
                             
   
});



