<?php
/* RegistrationModel Class
 * 
 * The model defines method canAdd to check if the email has been registered
 * Usually prepared SQL queries.
 * 
 * 
 */
class RegistrationModel extends Model 
    {
    
    /* canAdd method
     * 
     * @param $newEmail
     * 
     * Querys database to check if user's email is already entered
     * 
     * Returns: nothing
     * Throws: Exception if @newEmail is already in database
     * 
     */
    public function canAdd ($newEmail)
        {
        $SQL = "SELECT email
                  FROM Scientist
                  WHERE email LIKE :email";
         
        $statement = RegistrationModel::$database->prepare($SQL);  

        $statement->execute( array( ':email' => "%$newEmail%" ));
        
        if( $statement->rowCount() != 0)
            {
            throw new Exception("This email has already been Registerd");
            }
        else
            return;
            
        }   
    }
                  

