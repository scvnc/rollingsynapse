<?php

class MembershipModel extends Model {
      
      private $ROLES; // Will contain an array defining roles.    
      
      public function __construct () {
         
         // Need to call the parent constructor so that we don't completely override it.
         if (method_exists ("Model", "__construct"))
            parent::__construct();
         
         
         $this->ROLES = $this->loadRoles();
         
      }
      
      
      /* private loadRoles function
       *
       * 
       * Utility method used in the constructor.
       * Obtains all of the defined roles in the GroupRows table and
       * returnes a cross referenced array between idGroupRole and (role) name.
       */
      private function loadRoles() {
         
         $query = "SELECT *
                   FROM GroupRole
                   ORDER BY idGroupRole ASC";
               
         $statement = MembershipModel::$database->query($query);
         
         $roles = $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");
         
         
         
         $roleList = array();
         
         
         // Cross associate role name and role id's
         foreach ($roles as $role) {
            
            /* Example: > echo $roleList['admin'];
             *            0
             *            
             *          > echo $roleList[0];
             *            admin
             */
            
            $roleList[(int) $role->idGroupRole] = $role->name; 
            $roleList[$role->name] = (int) $role->idGroupRole;   
            
         }
         
         $this->rawROLES = $roles;
         return $roleList;
         
         
      }
      
      
      
      
      
      /* public role method
       * 
       * @param $arg
       *    Either an integer of the role number or string of a role name.
       * 
       * Returns an integer idGroupRole that correlates with a provided role name OR
       * returns a string of the group role nam that correlates with a provided idGroupRole.
       * 
       * If nothing matches, it returns -1
       */
      public function role($arg) {
        
            if (isset($this->ROLES[$arg]))
               return $this->ROLES[$arg];

            else
               return -1;
      }
      
      
      public function listRoles() {
         
         $roles = array();
         
         foreach ($this->ROLES as $key => $role) {
            
            if (is_numeric($key))
               $roles[$key] = $role;
            
         }
         
         return $roles;
         
      }
        
            
            
         
         

      
      
      
      
      public function roleLevel($group, $uid = NULL) //returns $uid's level of authentication for $group
/*returns a string if the user is found in a membership table or 0 on a failure to find (not logged in or not in the
membership table).  This function will recurse upwards through supergroups (if they exist) and check their membership 
tables as well [permissions are inherited].

default is the currently logged in user (unless otherwise specified)

            $group an idGroup from the Group table
            $uid is an idScientist from the scientist table

this function can be freely moved to another model, isAdmin and isRole depend on it*/
   {
      
      $am = new AuthenticationModel();
      
      // If a userid was not provided, then try using the username in the login session.
      if ($uid == NULL) {
         
         if (!$am->IsAuthenticated())
            return -1;
         else 
            $uid = $am->getAuthUID();
         
      }
      
      
      $query = "SELECT m.idGroupRole, gr.name 
                FROM Membership m, GroupRole gr 
                WHERE m.idGroup     = :group AND 
                      m.idScientist = :uid AND 
                      m.idGroupRole = gr.idGroupRole ";
                
      $statement = MembershipModel::$database->prepare($query);
      
      // Alternative way to pass in values for the psudo variables.
      $statement->bindValue(':group', $group, PDO::PARAM_INT);
      $statement->bindValue(':uid', $uid, PDO::PARAM_INT);
      
      $statement->execute();
      
      if($statement->rowcount() == 1) //the scientist is in the membership table and the associated role name is returned
      {
         $role = $statement->fetchObject();
         return (int) $role->idGroupRole;
      }
      else
      {
         $query = "SELECT idSuperGroup
                   FROM subGroup
                   WHERE idSubGroup = :group";
         $statement = MembershipModel::$database->prepare($query);
         $statement->execute(array(':group' => $group));
         
         if($statement->rowcount() == 1) //the statement returned a row (meaning there is a super group)
         {
            $super = $statement->fetchObject();
            
            //recursively call this function on the super group
            
            if ( $this->roleLevel($super->idSuperGroup) == $this->role("Admin") )
            return ( $this->roleLevel($super->idSuperGroup) == $this->role("Admin") ? 
                     $this->role("Admin") : -1 ); 
         }
         
      }
      
      // All else fails..
      return (int) -1; //user not found in the membership table
   }


   //this function needs testing (possibly revision)
   // NOT BEEN TESTED
   public function isAdmin($group, $uid = NULL)//returns true if the uid is an admin for the group (default current user)
   {
      
      $am = new AuthenticationModel();
      
      // If a userid was not provided, then try using the username in the login session.
      if ($uid == NULL) {
         
         if (!$am->IsAuthenticated())
            return FALSE;
         else 
            $uid = $am->getAuthUID();
         
      }
      
      return $this->roleLevel($group, $uid) == $this->role("Admin");
   }
   
   
   
   
}



?>
