<?php

class DeadlineModel extends Model {
	
	public function getProjectDeadline() {
		
		
		$secondDiff = mktime(14,0,0,5,2,2012)-time();
		
		if ($secondDiff < 0)
			return FALSE;
		
		
		$cdo = new countdown(array("s"=>$secondDiff));
		
		$cd = $cdo->get();
		
		$string = $cd['d'] + $cd['w']*7 . " day(s), " . $cd['h'] . " hour(s), " . $cd['i'] . " minute(s).";
		
		return $string;
		
	
	}
	
	
}

class countdown {
	private $time = array("y" => 0, "m" => 0, "w" => 0, "d" => 0, "h" => 0, "i" => 0, "s" => 0);
	private $offset = false;
	private $rel;

	function __construct($new_time, $rel = -1){
		if($rel < 0)
		{
			$this->rel = time();
		}
		else
		{
			$this->rel = $rel;
		}
		foreach($this->time as $key => $val)
		{
			if(isset($new_time[$key]))
			{
				$this->time[$key] = $new_time[$key];
			}
		}
		$this->time["i"] += floor($this->time["s"]/60);
		$this->time["s"] = $this->time["s"] % 60;

		$this->time["h"] += floor($this->time["i"]/60);
		$this->time["i"] = $this->time["i"] % 60;

		$this->time["d"] += floor($this->time["h"]/24);
		$this->time["h"] = $this->time["h"] % 24;

		$this->time["d"] += $this->time["w"]*7;
		$this->time["w"] = 0;

		$month = date("n", $this->rel);
		$year = date("Y", $this->rel);
		$date =  date("j", $this->rel);
		$leftover = date("t", mktime(0, 0, 0, $month, 1, $year)) - $date;
		$temp;
		if($this->time["d"] > $leftover)
		{
			$this->time["d"] -= $leftover;
			do
			{
				$month++;
				if($month > 12)
				{
					$year++;
					$month = 1;
				}
				$temp = $this->time["d"] - date("t", mktime(0, 0, 0, $month, 1, $year));
				if($temp >= 0)
				{
					$this->time["m"]++;
					if($this->time["m"] > 12)
					{
						$this->time["y"]++;
						$this->time["m"] = 1;
					}
					$this->time["d"] = $temp;
				}
			}
			while($temp > 0);
			if($this->time["d"] >= $date)
			{
				$this->time["d"] -= $date;
				$this->time["m"]++;
				$this->offset = true;
			}
			else
			{
				$this->time["d"] += $leftover;
			}
			$this->time["w"] += floor($this->time["d"]/7);
			$this->time["d"] = $this->time["d"] % 7;
		}

		$this->time["y"] += floor($this->time["m"]/12);
		$this->time["m"] = $this->time["m"] % 12;
	}

	public function get($time = array("y", "m", "w", "d", "h", "i", "s")){
		$temp = $this->time;
		if(!in_array("y", $time))
		{
			$temp["m"] += ($temp["y"]*12);
			$temp["y"] = 0;
		}
		if(!in_array("m", $time))
		{
			$month = date("n", $this->rel);
			$year = date("Y", $this->rel);
			if(!$this->offset)
			{
				$month++;
				if($month > 12)
				{
					$year++;
					$month = 1;
				}
			}
			for($i = 0; $i < $temp["m"]; $i++)
			{
				$temp["d"] += date("t", mktime(0, 0, 0, $month, 1, $year));
				$month++;
				if($month > 12)
				{
					$year++;
					$month = 1;
				}
			}
			$temp["m"] = 0;
		}
		if(!in_array("w", $time))
		{
			$temp["d"] += ($temp["w"]*7);
			$temp["w"] = 0;
		}
		else
		{
			$temp["w"] += floor($temp["d"]/7);
			$temp["d"] = $temp["d"] % 7;
		}
		if(!in_array("d", $time))
		{
			$temp["h"] += ($temp["d"]*24);
			$temp["d"] = 0;
		}
		if(!in_array("h", $time))
		{
			$temp["i"] += ($temp["h"]*60);
			$temp["h"] = 0;
		}
		if(!in_array("i", $time))
		{
			$temp["s"] += ($temp["i"]*60);
			$temp["i"] = 0;
		}
		if(!in_array("s", $time))
		{
			$temp["s"] = 0;
		}
		$ret = array();
		foreach($time as $val)
		{
			$ret[$val] = $temp[$val];
		}
		return $ret;
	}
}

?>