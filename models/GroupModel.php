<?php
/* GroupModel class
 * 
 * The model defines methods which access the database.
 * Usually prepared SQL queries.
 * 
 * GroupModel defines methods that return information about Groups.
 * 
 * 
 */
class GroupModel extends Model {
    
    
    /*
     * function viewAll()
     *
     * This function displays all the entries in the groups table
     */
    public function viewAll(){
    
        $SQL = "SELECT *
                 FROM `Group`";
                  
        $statement = GroupModel::$database->query($SQL);
        
        
        
        return $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");
    
    
    }
   
   
   
   public function viewAllJSON($name) {
      
      $SQL = "SELECT name as value
              FROM `Group`
              WHERE name LIKE :name";
              
      $stmnt = GroupModel::$database->prepare($SQL);
      $stmnt->execute( array('name' => "%$name%"));
      
      if ($stmnt->rowCount() == 0) {
         return NULL;
      }
      
      return $stmnt->fetchAll(PDO::FETCH_CLASS, "stdClass");
      
      
   }
    
    
    
    
    
    
    /* getSingle method
     * 
     * @param $id
     * 
     * Loads a specific group from the group table by specified id.
     * 
     * Returns: Object representing a row of this SQL Query.
     * Throws: Exception if a group by that id is not found.
     * 
     */
    public function getSingle($id) {
        
        $SQL = "SELECT *
                FROM `Group`
                WHERE idGroup = :id";
        
        $statement = GroupModel::$database->prepare($SQL);
        
        $statement->execute( array( ':id' => $id ) );
        
        if ($statement->rowCount() != 1)
            throw new Exception("<h3 align='center'>No group with that ID found.</h3>");
        
        
        return $statement->fetchObject();
        
    }

    
    /* getWorkflows method
     * 
     * @integer $id
     *    group ID
     * 
     * @bool $published
     *    Whether to get published workflows.
     * 
     * Finds all workflows belonging to a specific group.
     * 
     * Returns: Array of objects representing rows from the query.
     * Throws: Exception if no rows are returned.
     * 
     * TODO: Contemplate why this would be in the group model.
     *          For: because groups own workflows, and this is only listing workflow information
     *          Against: because in the end, it's returning rows from the workflow table.
     * 
     */
    public function getWorkflows($id , $published, $limit = NULL){
    
       $SQL = "SELECT *
                 FROM `Workflow`
                 WHERE idGroup = :id AND published = :published";
        
        if($limit == NULL){         
            $SQLLim = " ";}
        else{
            $SQLLim = " LIMIT :limit";
            $array[':limit']= $limit;}
                 
        $statement = GroupModel::$database->prepare($SQL.$SQLLim);
        
        $array[':id']= $id;
        $array[':published']= $published;
                
        $statement->execute( $array );
    
        if ($statement->rowCount() == 0)
            return 0;
       
        return $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");
    
    }
     
    
    /* isMember method  DEPRICATED!! USE THE METHODS IN MembershipModel
     * 
     * @integer $uid
     *    user ID in question
     * 
     * @integer $gid
     *    group ID to check membership with.
     * 
     * 
     * Returns: boolean; true if the specified user is a member of the group
     *                   false otherwise.
     */
    public function isMember( $uid , $gid){
    
        $SQL = "SELECT (COUNT(*) = 1) as isMember
                  FROM `Membership`
                  WHERE idGroup = :gid AND idScientist = :uid";
                  
        $statement = GroupModel::$database->prepare($SQL);
        
        $statement->execute( array( ':uid' => $uid , ':gid' => $gid) );
        
        return   (bool)$statement->fetchObject()->isMember;
                 
    
    
    }
    
    
    /*
     * getCreator function
     * 
     * Takes a group id as an arguement and returns the name of the person
     * that created the group
    */    
    public function getCreator($gid){
    
    $SQL = "SELECT s.name as creator
               FROM `Group` g, Scientist s
                WHERE g.idGroup = :idGroup AND
                       g.createdBy = s.idScientist";
           
        $statement = GroupModel::$database->prepare($SQL);  
        
        $statement->execute( array( ':idGroup' => $gid) );
        
        return $statement->fetchObject()->creator;
     
     }
    
    
    /*
     * canEdit function
     * 
     * Takes two arguements, the user id and a group id and checks if the user 
     * has permission to edit the group depending on the access level
    */    
    public function canEdit( $uid , $gid){
    
    $SQL = "SELECT (COUNT(*) = 1) as canEdit
             FROM Membership m
              WHERE idGroupRole = 0 AND idGroup = :gid 
                    AND idScientist = :uid";
    
    $statement = GroupModel::$database->prepare($SQL);
        
    $statement->execute( array( ':uid' => $uid , ':gid' => $gid) );
        
    return   (bool)$statement->fetchObject()->canEdit;        
    
    }
    
    
    
    /* getRoster method  TODO: Migrate this to MembershipModel eventually.
     * 
     * @param $gid
     *   The group id for which the member roster is to be retrieved.
     * 
     * (1) Returns NULL if there are no members.  ( There should techincally always be one ).
     * 
     * (2) Returns an array of objects representing a Member of the group.
     */
    public function getRoster($gid) {
        
        $SQL = "SELECT s.idScientist, 
                       s.name, 
                       m.idGroupRole, 
                       r.name as roleName,
                       m.joinDate
                FROM Membership m, Scientist s, GroupRole r
                
                WHERE m.idGroup     = :gid          AND
                      m.idScientist = s.idScientist AND
                      m.idGroupRole = r.idGroupRole;
                
                ORDER BY m.idGroupRole";
                
                
        $statement = GroupModel::$database->prepare($SQL);
        $statement->execute( array( ':gid' => $gid ) );
        
        /*
        if ($statement->rowCount() == 0)
            throw new RuntimeException("There are no members in this group...".
                                    "Something has gone terribly wrong...");*/
        
        
        return $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");
            
        
    }
    
   
   /* DEPRICATED!!! USE THE METHOD IN MembershipModel!!!
    * public getRoleList method
    * 
    * Returns an array of rows from the role table.
    */  
    public function getRoleList() {
        
        $SQL = "SELECT *
                FROM GroupRole";
        
        $statement = GroupModel::$database->query($SQL);
        
        if ($statement->rowCount() == 0)
            throw new Exception("There are no roles defined!");
        
        return $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");
        
    }
    
    
    
    /* addMember method  TODO: MOVE THIS TO THE MEMBERSHIP MODEL.
     * 
     * @param $uid
     *     The UserID to add into the group
     * 
     * @param $gid
     *  The groupID to add a user into.
     * 
     * Inserts the specified user into a specified group.
     * 
     * Returns bool true on success.
     */
    public function addMember($uid, $gid) {
         
      
        $SQL = "INSERT INTO `Membership`
                       ( `idGroup`, `idScientist`, `joinDate`, `idGroupRole` )
                 VALUES(      :gid,          :uid,      now(),             2 )";
                 
        try {
        $statement = GroupModel::$database->prepare($SQL);
        $statement->execute( array( ':gid' => $gid, 
                                    ':uid' => $uid ) );
      } catch (PDOException $e) {
         if ($e->getCode() == 23000)
            throw new RuntimeException("User $uid already a member in group $gid.");
         
         throw $e;
      }
        
        if ($statement->rowCount() != 1)
            throw RuntimeException("No rows affected");
        
        return TRUE;
    }
    
   
   
    /* delMember method TODO: MOVE THIS TO THE MEMBERSHIP MODEL.
     * 
     * @param $uid 
    *    The userID to remove from group.
    * 
    * @param $gid
    *    The group to remove the user from.
    * 
    * Revokes membership of a group from a user.
    * 
    */
    public function delMember($uid, $gid) {
        
      /*****************************************************
       * ACCESS CONTROL: is the authenticated user an admin for 
       * this group? OR is the user trying to remove themself?  Perhaps place this in controller.
       *****************************************************/                  

      // Will removing this user make the group unmanagable?
      if ( !$this->checkAdminRemoval($uid, $gid) )
         throw new RuntimeException("Cannot remove user as it would ".
                                    "make the group admin-free.");
      
      
      
      
        /*****************************************
       * CHECKS OK: Actually delete membership.
       *****************************************/
        
        $SQL = "DELETE FROM `Membership`
              WHERE idScientist = :uid AND
                    idGroup     = :gid";
                 
            
        $statement = GroupModel::$database->prepare($SQL);
        $statement->execute( array( ':gid' => $gid, 
                                  ':uid' => $uid ) );
        
        if ($statement->rowCount() == 0)
            throw new RuntimeException("No rows were affected. Perhaps that membership doesn't exist?");
        
        return TRUE;
    }
    
   
   
   /* public changeMemberRole function TODO: MOVE THIS TO THE MEMBERSHIP MODEL.
    * 
    * Changes the role of a group member.
    * 
    * @param $uid
    *    The user id to modify role.
    * 
    * @param $gid
    *    The group id that the user is a member of
    * 
    * @param $rid
    *    The role id that will be changed.
    *    0 - Admin
    *    1 - Collaborator
    *    2 - Member
    * 
    * 
    * Returns true on success.
    */
    public function changeMemberRole($uid, $gid, $rid) {
      
      // Would changing this user's role from admin make the group unmanagable?
      if ( !$this->checkAdminRemoval($uid, $gid) )
         throw new RuntimeException("Cannot remove user as it would ".
                                    "make the group admin-free.");
      
      $SQL = "UPDATE `Membership`
              SET    `idGroupRole` = :rid
              WHERE  `idGroup`     = :gid AND
                     `idScientist` = :uid";
      
      $statement = GroupModel::$database->prepare($SQL);
      $statement->execute( array(':rid' => $rid,
                                 ':gid' => $gid,
                                 ':uid' => $uid ) );
      
      if ($statement->rowCount() == 0)
         throw new RuntimeException("No role was modified. Either the user" .
                                    " is not a member of this group or already".
                                    " has the specified role.");
      
      return TRUE;
                                             
    }
   
   
   
   /* private checkAdminRemoval method TODO: MOVE THIS TO THE MEMBERSHIP MODEL.
    * 
    * Checks to see if by removing this ($uid) it would make the 
    * group ($gid) unmanagable because there would be no admins.
    * 
    * Returns true if it's OK to remove this admin.
    * 
    */  
    private function checkAdminRemoval ($uid, $gid) {
       
       $mMod = new MembershipModel();
          
       
       // Construct SQL query that would check how many admins are in the group
      // and if this user is an admin.
      $isAdminSQL = "SELECT (count(*) = 1)
                     FROM Membership
                     WHERE idScientist = :uid AND
                           idGroup     = :gid AND
                           idGroupRole = :role";

      $SQL = "SELECT count(*)              as groupAdminCount, 
                     (" . $isAdminSQL . ") as isAdmin
              FROM Membership
              WHERE idGroup     = :gid AND
                    idGroupRole = :role";
            
      $statement = GroupModel::$database->prepare($SQL);
      $statement->execute( array( ':gid'  => $gid, 
                                  ':uid'  => $uid, 
                                  ':role' => $mMod->role("Admin") )
                            );
      
      $adminState = $statement->fetchObject();
      
      // If the this user is an admin, and it's the only admin, return false.
      return !( $adminState->isAdmin         == true &&
                $adminState->groupAdminCount <= 1 );
            
      
    }
    
    
    
    public function role($uid , $gid){ 
    
        $SQL = "SELECT g.name
                    FROM `Membership` m, `GroupRole` g
                    WHERE m.idGroupRole = g.idGroupRole AND
                    m.idScientist = :uid AND m.idGroup = :gid";
     
        $statement = GroupModel::$database->prepare($SQL);
        
    $statement->execute( array( ':uid' => $uid , ':gid' => $gid) );

     return $statement->fetchObject()->name;
    }
    
    
    
    /*
     * funtion viewGroups
     *
     *   Queries all the groups each logged in user is a member of
     */
    public function viewGroups(){
    
      if(isset($_SESSION['uid'])){    
    
        $SQL="SELECT * 
                FROM `Group` g , `Membership` m
                    WHERE g.idGroup = m.idGroup AND m.idScientist = :sid";
             
        $statement = GroupModel::$database->prepare($SQL);

        $array[':sid'] = $_SESSION['uid'];    
    
    
        $statement->execute( $array );
    
    
        return $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");
      }

    }
   
   
   
    /*
    * function isPrivateGroup()
    *
    * This function takes a group id as an arguement and it checks if the
    * group is private(closed) or public(open)
    */   
    public function isPrivateGroup($gid) {
   
   
        $SQL = "SELECT private
                    FROM `Group`
                    WHERE idGroup = :id";
   
   
        $statement = GroupModel::$database->prepare($SQL);

        $array[':id'] = $gid; 
   
        $statement->execute( $array );
   
   return (bool) $statememt->fetchObject()->private;
}
    

    public function groupID($name) {
      
      $SQL = "SELECT idGroup
           FROM `Group`
           WHERE name = :name";
   
   
      $statement = GroupModel::$database->prepare($SQL);

      $array[':name'] = $name; 
   
      $statement->execute( $array );
      
      return ($statement->rowCount() != 1) ? NULL: $statement->fetchObject()->idGroup;
      
    }
   
   
   public function create($name, $sDesc, $lDesc, $private = FALSE, $superGID = NULL, $creatorID = NULL) {
      
      $am = new AuthenticationModel();
      $mm = new MembershipModel();
      
      if($creatorID == NULL)
         $creatorID = $am->getAuthUID();
      
      
      
      if($creatorID == -1)
         throw new RuntimeException("Need creator ID!!");
      
      
      
      /*
       * Prepare relation table (Membership or subGroup)
       */
      
      // Either
      // (1) Make the subgroup relation..
      if($superGID != NULL) { 
         
         try {
            echo $superGID;
            $this->getSingle($superGID);
            
         } catch (Exception $e) {
            throw new RuntimeException("When checking superGroup: ". $e->getMessage());
         }
         $sql = "INSERT INTO subGroup(idSubGroup, idSuperGroup)
                           VALUES(last_insert_id(), :supID)";
         
         $relationQuery = GroupModel::$database->prepare($sql);
         
         $relationQuery->bindParam(':supID', $superGID, PDO::PARAM_INT);
         
         
      // Or
      // (2) Make this user the admin role...
      } else {
         
         $sql  = "INSERT INTO `Membership`
                  (`idGroup`,
                   `idScientist`,
                   `joinDate`,
                   `idGroupRole`)
                  VALUES ( last_insert_id(),
                           :sid,
                           now(),
                           ".$mm->role("Admin")." )";
                           
         $relationQuery = GroupModel::$database->prepare($sql);
         
         $relationQuery->bindParam(':sid', $creatorID, PDO::PARAM_INT);
         
      }
      
      /*
       * Prepare group table insertion.
       */
      $sql = "INSERT INTO `Group`
                (`name`,
                 `shortDesc`,
                 `longDesc`,
                 `private`,
                 `createDate`,
                 `createdBy`)
                VALUES
                 (:name,
                  :shortDesc,
                  :longDesc,
                  :private,
                  now() ,
                  :creatorId);";
      
      $createQuery = GroupModel::$database->prepare($sql);
      
      $createQuery->bindParam(':name', $name);
      $createQuery->bindParam(':shortDesc', $sDesc);
      $createQuery->bindParam(':longDesc', $lDesc);
      $createQuery->bindParam(':private', $private);
      $createQuery->bindParam(':creatorId', $creatorID);
      
      
      // Attempt group creation.
      GroupModel::$database->beginTransaction();
      
      $createQuery->execute();
      $relationQuery->execute();
      
      GroupModel::$database->commit();
      
      
      
      
   }
   
    /* viewFamilyGroups method
     * 
     * Querys Database to find if current group has a SuperGroup
     *  
     */
   public function viewFamilyGroups($groupID)
    {
  
    $SQL="SELECT g.name, g.idGroup 
            FROM `Group` g, subGroup s
            WHERE :subGroup = s.idSubGroup AND s.idSuperGroup = g.idGroup";
             
    $statement = GroupModel::$database->prepare($SQL);

    $array[':subGroup'] = $groupID;    
    
    
    $statement->execute( $array );
    
    //if there is a Super Group
    if( $statement->rowCount() > 0)
        {
        return $statement->fetchObject();
        }
    
    return false;
    }
   
   /*
           $array[':name'] = $_POST['name'];
        $array[':shortDesc'] = $_POST['shortDesc'];
        $array[':longDesc'] = $_POST['longDesc'];
        $array[':private'] = isset($_POST['private']) ? true : false;
        $array[':creatorId'] = $_SESSION['uid'];
        $array[':sid'] = $_SESSION['uid'];
        $array[':role'] = '0';
    */
    
    
    
    
    
    
    
    
}
