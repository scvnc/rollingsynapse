<?php 
class ScientistModel extends Model {
   
   public function findID($name) {
      
      $SQL = "SELECT name as label, email, idScientist as value
              FROM Scientist
              WHERE name LIKE :name";
              
      $stmnt = ScientistModel::$database->prepare($SQL);
      $stmnt->execute( array('name' => "%$name%"));
      
      if ($stmnt->rowCount() == 0) {
         return NULL;
      }
      
      return $stmnt->fetchAll(PDO::FETCH_CLASS, "stdClass");
      
   }
   
   
   
}
