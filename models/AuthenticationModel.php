<?php
/* AuthenticationModel class
 * 
 * The model defines methods which access the database.
 * Usually prepared SQL queries.
 * 
 * AuthenticationModel defines methods that return information about Authentications.
 * Could include access control methods potentially.
 * 
 * 
 */
class AuthenticationModel extends Model {
	
	public function register( /* .. */ ) {
		
		
		
	}
	
   public function getAuthUID() {
      
      if ($this->IsAuthenticated())
         return $_SESSION['uid'];
      
      else
         return NULL;
      
   }
   
	public function matchCredentials($un, $pw) //checks credentials, returns an object of that user if they are
	{
				//"SELECT s.idScientist, p.nameFirst, p.nameLast
		$query = "SELECT idScientist, name
					FROM Scientist
					WHERE email = :un AND password = SHA1(:pw)";
		
		$statement = AuthenticationModel::$database->prepare($query);
		$statement->execute( array(':un' => $un, ':pw' => $pw) );
		
		
		if($statement->rowcount() == 1)return $statement->fetchObject(); //found one and only 1
		else return 0; //not found or more than 1, return something empty
	}
	
	public function IsAuthenticated() //checks if the user is authenticated or not, returns bool
	{
		if(isset($_SESSION['uid']))return true;
		else return false;
	}

	public function logout() //unsets all the session variables tracking the user logged in, returns void
	{
		unset($_SESSION['uid']);
		/*
		unset($_SESSION['firstname']);
		unset($_SESSION['lastname']);
		*/
		unset($_SESSION['name']);
	}
	

	
}
?>
