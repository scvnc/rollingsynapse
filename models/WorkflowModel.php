<?php
/* WorkflowModel class
 * 
 * The model defines methods which access the database.
 * Usually prepared SQL queries.
 * 
 * WorkflowModel defines methods that return information about Workflows.
 * 
 * 
 */
class WorkflowModel extends Model {
	
/*
	Function: ViewWorkflow
	Purpose:  This function retrieves a single Workflow
			  and displays it.
	Arguments: The ID of a workflow

*/	
	
	
	public function viewWorkflow($idWorkflow) {
	
			$SQL = "
			SELECT * 
			FROM Workflow w
			WHERE w.idWorkflow = :idWorkflow";
			
			$statement = WorkflowModel::$database->prepare($SQL);
		
		$statement->execute( array( ':idWorkflow' => $idWorkflow) );
		
		if ($statement->rowCount() != 1)
			throw new Exception("No Workflow with that ID found.");
		
		
		return $statement->fetchObject();
			
			
	
	
	}
	
	/*  
	Function: viewAll
	Purpose: This function shows all the published workflows
			 of groups. Acts as a main page for the Workflows Tab.
	Arguments: Takes no parameters
	
	*/
	

	public function viewAll() {
	
			$SQL = "SELECT * 
					FROM `Workflow`
					WHERE Published = '1'";
			
			
			$statement = WorkflowModel::$database->prepare($SQL);
			return $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");
	
	}
	

/*




*/
public function isMember( $uid , $gid){
	
	    $SQL = "SELECT (COUNT(*) = 1) as isMember
				  FROM `Membership`
				  WHERE idGroup = :gid AND idScientist = :uid";
				  
		$statement = GroupModel::$database->prepare($SQL);
		
		$statement->execute( array( ':uid' => $uid , ':gid' => $gid) );
		
		return   (bool)$statement->fetchObject()->isMember;
				 
	
	
	}
		
	
	/**************************
	 * ACCESS CONTROL METHODS *
	 **************************/
	
	/* canEdit method
	 * 
	 * @param $idWorkflow
	 *   Workflow ID
	 * @param $idScientist
	 *   Scientist ID
	 * 
	 * Returns true if the specified userID is allowed to edit the specified workflow ID;
	 *         otherwise false.
	 */
	public function canEdit( $idWorkflow, $idScientist ) {
		
		
		// This SQL statement returns a column called canEdit
		// if true, then the specified user can edit the workflow in question.
		$SQL = "
		SELECT (count(*) = 1) as canEdit
		FROM Workflow w, Membership m
		WHERE w.idWorkflow = :edit AND 
      	      m.idScientist = :scientist AND
              w.idGroup = m.idGroup AND
              m.idGroupRole <= 1;     # Moderator = 0, Collaborator = 1
              ";
		
		$query = WorkflowModel::$database->prepare($SQL);
		
		$query->execute(array(':edit' => $idWorkflow, ':scientist' => $idScientist));
		
		
		return (bool) $query->fetchObject()->canEdit;
		
	}
   
   /* get method
    * 
    * Dynamically builds a SQL statement that retrieves Workflows.
    * The arguments add additional constraints to the SQL statement.
    * 
    * @int $wid
    *    Adds WHERE idWorkflow = $wid
    * 
    * @int $gid
    *    Adds WHERE idGroup = $gid
    * 
    * @bool $published
    *    Adds WHERE published = $published
    * 
    * @int $limit
    *    Adds LIMIT $limit for limiting the number of rows returned.
    * 
    * @string $orderby  *******NOT IMPLEMENTED*****
    *    Adds ORDER BY $orderby.
    *    Example: $orderby = "pubDate ASC";
    * 
    * 
    */
   public function get($wid = NULL, $gid = NULL,  $published = NULL, $limit = NULL, $orderby = NULL) {
      
      // BASE SQL STATEMENT
      $SQL = "SELECT *
              FROM `Workflow`";
      
      // Declare supplimentary portion.
      $SQL2 = "";
      
      
      /* CHECK TO SEE IF EACH ARGUMENT IS NOT NULL, 
       * THEN APPLY IT TO THE QUERY SOMEHOW */
      
      
      /*==========================================================*
       * Begin constructing the WHERE clause.                     *
       *==========================================================*
       * If there is a where clause, then stick it into an array  *
       * that we will loop through later on...                    */
      
      if ($wid != NULL) {
      
         $where_stmnt[] = "idWorkflow = :wid";
         $prepArray[':wid'] = $wid;
      
      }


      if ($gid != NULL) {
         
         $where_stmnt[] = "idGroup = :gid";
         $prepArray[':gid'] = $gid;
         
      }
      
      if ($published != NULL) {
         
         $where_stmnt[] = "published = :pub";
         $prepArray[':pub'] = (bool) $published;
         
      }
      
      // Glue the where clauses together.

      if (isset ($where_stmnt)) {
         
         $SQL2 .= " WHERE ";
         
         $numStmnts = sizeof($where_stmnt);
         
         // For each in the array.
         for ($i = 0; $i < $numStmnts; $i++) {
            
            // Tack on the where clause to the SQL statement
            $SQL2 .= ' '.$where_stmnt[$i];
            
            /* If this isn't the last where clause in the array
             * then tack on 'AND' to the query */            
            if ($i != $numStmnts-1)
               $SQL2 .= " AND ";
            
         }
         
         
         if ($orderby != NULL) {
            
            // NOT IMPLEMENTED
         }
         
         
         // Tack on the LIMIT constraint
         if ($limit != NULL) {
            
            if (!is_numeric($limit))
               throw new RuntimeException("Provided limit is not an integer!");
            
            $SQL2 .= " LIMIT $limit";
            
         }
         
         
      }
      
      
      //echo $SQL.$SQL2;
      $statement = GroupModel::$database->prepare($SQL.$SQL2);
      
      $statement->execute( (isset($prepArray)) ? $prepArray : NULL );
   
      if ($statement->rowCount() == 0)
         return 0;
      
      return $statement->fetchAll(PDO::FETCH_CLASS, "stdClass");

   }
 
/*
Function: getGroupID
Purpose: Obtains the ID of a Group from the ID 
		 of a Workflow
Arguments: The ID of a workflow
*/ 
   
	
public function getGroupID($idWorkflow){

	
	$SQL = "SELECT `idGroup`
	FROM `Workflow`
	WHERE `idWorkflow` = :id"; 

	$statement = WorkflowModel::$database->prepare($SQL);
   
   $statement->execute(array(':id' => $idWorkflow));
	
	
   return $statement->fetchObject()->idGroup;
	
}
	
	
}
