<?php
/* Router Class
 * 
 * This object has the job of interpreting URL requests and routing them to a 
 * controller and/or one of it's methods.
 * 
 * Example case:
 *    The user navigates to http://example.com/index.php?workflow/2
 *    The router can interpret this url and execute the Workflow's view method
 *    with 2 as an argument... maybe id number 2?
 * 
 */

class Router {
   

   public function __construct() {

      $args = $this->get_args();
   
   
      /*----------------------------------------------*
       * Figure out which controller we're waking up. *
       * ---------------------------------------------*/
      
      // Case: no arguments
      if (sizeof($args) == 0)
            $controllerName = "MainController"; // DEFAULT CONTROLLER?
      
      
      
      // Case: try first argument as controller name
      else {
      
         // POTENTIAL SECURITY BREACH.. This should be sanitized here or in the argument parsing.
         
         //Make sure the proposed controller name has the first letter capitalized.
         $args[0][0] = strtoupper($args[0][0]);
         
         
         $controllerName = $args[0]."Controller";
         
         
         array_shift($args);    // Pops the first element and shifts things down.
         
      
      }
      
      
      
      
      /*--------------------------------------------------
       *  Instantiate the controller determined to wake.
       * -------------------------------------------------*/
       
      $activeController = new $controllerName($args);  
      // (Yes this is variable-as-a-class-name thing is pretty wacky)
      
      
      
      
      /** Now figure out which method of the controller we're going to launch next. 
       * 
       *  We may need to do this in the controller itself.
       * **/
      
      
      $viewMethod = ( sizeof($args) != 0 ) ? $args[0] : "main";
      
      try {
      	
	      if ( method_exists ( $activeController , $viewMethod ) )
	        $activeController->$viewMethod($args);
	      
	      
	      elseif ( method_exists ( $activeController , "main" ) )
	        $activeController->main($args);
	      
	        
	      else 
	         throw new Exception("Fatal: cannot find a suitable controller view/method!");
		  
	  
	  } catch (ExitException $e) { } // Thrown if a method wants to jump out of the call stack.
      
      


      
   }
   

   private function get_args() {
      
      // Obtain the URL Arguments
      
      if ( preg_match('/\?(.*)$/', $_SERVER['REQUEST_URI'], $results) != 0 ) 
         $args = $results[1];
      
      else
         return array(); 
         
      
      /** Parse the elements of the URL **/
      
      $args = explode ('/', $args); // kind of like 'split' in awk for those in 330
      
      
      
      $finalArgs = array();
      // Rid of empty arguments that may be here.
      for ($i = 0; $i < sizeof($args); $i++) {
         
         // if empty
         if ($args[$i] != '') {
            $finalArgs[] = $args[$i];
         }
         
         // TODO: Any other sanitation?
      }
      
      return $finalArgs;
      
      
   }
   
   
   
   
}

?>
