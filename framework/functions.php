<?php

/* PHP Class Autoloader
 * 
 * PHP has this strange and cool function that is called whenever an unknown
 * class is asked to instantiate.  Call it a pre-constructor? :)
 * 
 * This has been defined to check an array of potential locations
 * and load the file which matches.
 * 
 */
function __autoload($classname) {

    $possible_locations = array( "./framework/".$classname.".php", 
                                 "./controllers/".$classname.".php",
                                 "./models/".$classname.".php",
                                 "./helpers/".$classname.".php" );
    
    foreach ($possible_locations as $classfile) {
       
      if ( file_exists($classfile) ) {
         include_once($classfile);
         return;
      }
      
   }
   
   // At this point if nothing has loaded, this has failed.
   
   
   throw new Exception("Fatal: Could not find definition of $classname");
    
}

?>
