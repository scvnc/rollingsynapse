<?php
abstract class Controller {

   
   protected static $APP_ROOT; // Variable which references where this website is installed.
   
   private $title; // Title of the page;
   
   public function __construct($args) {
      
       global $_CONFIG;
       
	   $this->title = "Rolling Synapse"; // HARDCODED TITLE OF SITE
	   
	   /* Bring in the arguments from the url/router
	      example.com/controller/
	      Method Name: $this->args[0]
	      First argument: $this->args[1]
	      number of arguments: sizeof($this->args); */
	   $this->args = $args;
	   
	   // Give this object an identiy.
       $this->controllerName = str_replace("Controller", "", get_class($this));
       
       
       
	   // For fun and perspective: get remaining time till completion.
	   $dlm = new DeadlineModel();
	   $this->deadline = $dlm->getProjectDeadline();
       

       
       
       
   }
   
   
   function __destruct() {
   	

      
      
      
      
   }
   
   
   
   /* Abstract public main method
    * 
    * [This must be defined in every derived controller.]
    * 
    * The main method is what the Router will call by default.
    * 
    */
   abstract public function main(); 
   
   
   
   
   
   /* Protected deploy method
    * 
    * @param $layoutFolder
    * 	o Which folder inside views contains header.html and footer.html ?
    * 	o Pass in NULL to not load a header or footer (useful for ajax calls)
    *   o Defaults to 'layout'.
    * 	
    * 
    * This method deploys the page by loading the specified
    * 	(1) views/$layoutFolder/header.html
    *   (2) The calling method's view ( views/[controller]/[method].html )
    *   (3) views/$layoutFolder/footer.html
    * 
    * Recommended to call whenever you are ready to display the page.
    * 
    */
   protected function deploy($layoutFolder = "layout") {
      
      global $_CONFIG;
	  
      // Turn data members of this controller into local variables
      foreach ($this as $key => $variable) {
         ${$key} = $variable;
      }
      
      
      
      $viewName = $this->getViewName();
      
	  // Show Header
	  if ($layoutFolder) {
      	$file = $_CONFIG['APP_ROOT']."/views/".$layoutFolder."/header.html";
		  
      	if (file_exists($file))
      		include($file);
		else
			throw new Exception("Can not find views/$layoutFolder/header.html !");
		
      }
	  
      // Begin a list of where we might find the view file for this controller method.
      $potentialViewFile = array( "views/".strtolower($this->controllerName)."/".$viewName.".html");
      
      // Add the parent class's view folder as a search possibility if it makes sense.
      if (($parentController = get_parent_class($this)) != "Controller")
         $potentialViewFile[] = "views/".strtolower(str_replace("Controller", "", $parentController)."/".$viewName).".html";
      
      
      
      
      // Begin the search and include the file if found.
      $found = false;
      foreach ($potentialViewFile as $f){
         
         if (file_exists($f)) {
            include ($f);
            $found = true;
            break;
         }
            
      }
      
      if ($found == false) 
           throw new Exception("Fatal: $this->controllerName"."Controller's $viewName method does not have coorisponding view file!");
         
      // Show Footer
	  if ($layoutFolder) {
      	$file = $_CONFIG['APP_ROOT']."/views/".$layoutFolder."/footer.html";
		  
      	if (file_exists($file))
      		include($file);
		else
			throw new Exception("Can not find views/$layoutFolder/footer.html !");
		
      }
	  
	  // This will be caught by the router, think of it like a mega-return.
	  throw new ExitException();
      
   }
   
   /* Public setTitle method
    * 
    * @param $title
    * 	The page title you would like to be accessible as $title on views.
    * 
    * Prepends the argument on to the controller title data member.
    */
   public function setTitle($title) {
   		
		$this->title = $title . " - " . $this->title;

   }
   
   
   /* private getViewName Method
    * 
    * A very sly way to look back 2 calls to see which method called the function
    * we're in.
    * 
    * Credit goes to http://stackoverflow.com/a/9133897
    * 
    * 
    * Returns: String of the calling function
    * 
    */
   protected function getViewName() {
   
       $e = new Exception();
       $trace = $e->getTrace();
       //position 0 would be the line that called this function so we ignore it
       $last_call = $trace[2];
       
       return $last_call['function'];

   }
   
   /* protected getArg method
    * 
    * Example request: ?controller/method/vince/3
    * 
    * @param $num
    *   The position of the argument.
    *       $name = $this->getArg(1); // Returns "vince"
    *      
    * @param $numeric
    *    Boolean whether to check if the argument is numeric.
    *       $page = $this->getArg(1, true); // Throws exception
    * 
    * Obtains and validates an argument. Throws exception if the
    * argument does not exist or fails validation checks.
    * 
    */
   protected function getArg($num, $numeric = false){

      
      if (sizeof($this->args) <= $num)
         throw new Exception("Argument $num not provided in URL.");
      
      if ($numeric) {
         if (!is_numeric($this->args[$num]))
            throw new Exception ("Argument $num is not numeric.");
      }
      
      return $this->args[$num];     
   
      
   }
   
   
   
   

   
   
}
?>
