<?php
require("config.php");

abstract class Model {
	
	static public $database;
	
	
	
}

Model::$database =  new PDO("mysql:host=".$_CONFIG['dbHost'].";dbname=".$_CONFIG['dbSchema'], 
                             $_CONFIG['dbUser'], 
                             $_CONFIG['dbPassword']);

// Have PDO throw exceptions on SQL errors.
Model::$database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);